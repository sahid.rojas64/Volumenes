import math
def volumen_esfera(radio):
 """""
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera :Ing. Computadores 
Curso: Intro a la programación
Como implementar: -
Programa : -
Autores : Sahid Rojas Chacon y Jose Solano Mora
Lenguaje: Python 3.6
Version : 1.0
Ult.Fecha de mod: 26/4/18
Entradas : Radio de una esfera  y es un numero 
Restricciones: el radio tiene que ser positivo
Salidas: el volumen de una esfera"""
 area=(4/3)*math.pi*radio**2
 return area

def volumen_piramide(H,L):
 """""
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera :Ing. Computadores 
Curso: Intro a la programación
Como implementar: -
Programa : -
Autores : Sahid Rojas Chacon y Jose Solano Mora
Lenguaje: Python 3.6
Version : 1.0
Ult.Fecha de mod: 26/4/18
Entradas : altura de una piramide  y el lado de esa esfera 
Restricciones: el radio tiene que ser positivo
Salidas: el volumen de una piramide con base cuadrada"""
 volumen=((L*L)*H)/3
 return volumen

def volumen_cil(H,R):
    from math import pi
    Vol=H*pi*R**2
    return Vol

